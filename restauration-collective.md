# Réduire la pollution de nos cantines, beaucoup et facilement

Nous pouvons réduire notre impact environnemental de façon
*significative* (un changement avec un vrai impact) par un changement
*simple* (aucune difficulté de mise en oeuvre) à notre
alimentation. En particulier, nos *restaurations collectives*
(cantines, restaurants, etc.) peuvent jouer un rôle déterminant, en
appliquant ces mesures à leurs centaines de repas servis, tout en
informant et sensibilisant les usagers.

On a tous entendu dire que les viandes et les poissons sont beaucoup
plus polluants, à apports nutritionnels équivalents, que les régimes à
base de végétaux. Passer à un régime végétarien pour diminuer son
impact environnemental est une excellente décision, mais qui change
beaucoup d'habitudes et demande de la préparation. Ce n'est pas ce que
nous proposons ici !

Ce que vous ignorez peut-être (beaucoup de gens l'ignorent), c'est
qu'au sein des viandes il y a de *très fortes* différences d'impact
environnement. Les viandes rouges (par exemple le boeuf, le veau et
l'agneau) polluent plus de *10 fois plus* que le poulet et les autres
volailles (dinde, canard...), et 5 à 10 fois plus que le porc. Ces
viandes rouges sont la cause *principale* de pollution dans votre
assiette. Contrairement aux idées reçues, les viandes rouges polluent
*beaucoup plus* que de faire pousser des tomates sous serre, de faire
venir de loin des bananes en avion ou des fraises en camion, ou même
d'utiliser chaque jour de nouveaux couverts en plastique jetable !

### Proposition

Nous proposons aux lieux de restauration collective d'arrêter de
servir des viandes rouges. Plus de boeuf, veau ou agneau à la
cantine. Tant mieux si les gens mangent plus de repas végétariens,
mais le poisson, les volailles et le porc sont aussi disponibles.

Cette proposition est simple parce qu'elle ne change que très peu nos
habitudes alimentaires.[^santé] Amateurs de steak-frites à la cantine,
prenez le poulet-frites. Du point de vue des cuisines, il n'y a pas
besoin de proposer immédiatement de nouvelles recettes, de trouver de
nouveaux circuits d'approvisionnement ou de se former sur de nouveaux
produits. (Même si ce serait super de proposer plus de recettes
végétariennes et végataliennes diversifiées, il y a de plus en plus de
personnes qui mangent végétarien et ont rarement une bonne expérience
en restauration collective.)

Alors, dès le mois prochain, plus de viande rouge à la cantine ?

[^santé]: Au sujet de la santé, des apports nutritionnels, passer de
la viande rouge à la viande blanche ne pose pas de problème
particulier. Voir par exemple [cet
article](https://myeasysante.fr/news/viandes-rouges-viandes-blanches-lesquelles-sont-meilleures-pour-la-sante/). La
viande rouge est par ailleurs probablement associée à un risque accru
de cancer; le Fonds Mondial de Recherche contre le Cancer recommande
d'en manger de façon "modérée et occasionnelle", et en particulier de
ne pas dépasser 300g par semaine, soit deux portions. Il n'y a pas de
besoin nutritionnel spécifique en viande rouge que la restauration
collective serait en devoir de remplir.

### Quelques chiffres explicatifs

Une personne vivant en France produit en moyenne 9 tonnes
équivalent-CO2 par an (CO2eq: c'est l'unité de pollution qui mesure
l'impact sur le réchauffement climatique) [^empreinte-moyenne]. Pour
respecter nos engagements sur le réchauffement climatique (limiter la
hausse à 2 degrés en 2100), il faudrait rescendre à 2 tonnes par
personne. Il faut diviser par *5* les émissions individuelles de gaz
à effet de serre !

[^empreinte-moyenne]: https://www.carbone4.com/myco2-empreinte-moyenne-evolution-methodo

Sur ces 9.0 tonnes/an équivalent-CO2, la principale source d'émissions
individuelles en moyenne vient des transports (2.7 tonnes/an
équivalent-CO2), suivie de l'alimentation (2.4 tonnes/an), le logement
(1.9 tonnes/an), les objets qu'on achète ou qu'on utilise (1.6
tonnes/an). L'alimentation représente donc un bon quart des émissions,
encore plus pour les personnes utilisant des transports peu polluants
(transports en commun plutôt que voiture individuelle). Au niveau
mondial, le projet Drawdown liste les [principaux leviers
d'action](https://drawdown.org/solutions/table-of-solutions) pour
rester sous 2 degrés de réchauffement, et les deux plus importants
sont liés à des changements de pratiques alimentaires --
l'alimentation est un poste de coût important où il reste de très
fortes marges de progrès.

Avec les filières d'approvisionnement moyennes en France, produire un
kilo de lentilles dans votre assiette produit 500 grammes
à 1 kilogramme de pollution équivalent-CO2 -- comme la plupart des
fruits et des légumes[^incertitudes]. Un kilo de poulet, c'est 3kg
à 5kg équivalent-CO2. Un kilo de boeuf, c'est 30kg à 60kg
équivalent-CO2 !  C'est 10 fois plus que le poulet, et ça écrase
largement toutes les autre sources de pollution dans votre assiette.

[^incertitudes]: les incertitudes sont importantes sur les calculs
d'impact carbonne, mais les différentes études et méthodes de calcul
produisent des résultats cohérents avec des proportions comparables
entre les différentes sources d'émission.

Pour comparer[^ademe]: un (petit) steak de 150g de boeuf, c'est
environ 5kg de pollution équivalent-CO2. Ça pollue autant qu'environ
10 portions équivalentes de pois chiches, environ un kilogramme de
bananes acheminées en avion, environ 23km en voiture en milieu
urbain, environ 2000km en TGV, ou environ 400 lots de couverts
en plastique jetable. Manger du boeuf est peut-être votre action
individuelle facilement remplaçable la plus polluante de la journée.

[^ademe]: En utilisant [les données](https://bilans-ges.ademe.fr/fr/basecarbone/donnees-consulter/) de l'ADEME.

Si vous voulez aller plus loin, voici ci-dessous des classements de
l'impact équivalent-CO2 des aliments les plus courants. Les nombres
sont un peu différents (l'impact dépend du pays et de ses pratiques
agricoles, et la méthode de calcul influe aussi sur les résultats),
mais les résultats sont toujours du même ordre et amènent à la même
conclusion.

- [par kg de nourriture produite](https://ourworldindata.org/environmental-impacts-of-food)
- [par quantité de protéines](https://ourworldindata.org/grapher/ghg-per-protein-poore)
- [par quantité de calories](https://ourworldindata.org/grapher/ghg-kcal-poore)


### Conclusion

Passer de ~10 tonnes équivalent-CO2 par an et par personne à ~2
tonnes, pour limiter l'impact du réchauffement climatique, c'est un
défi majeur qui demandera des changements importants de notre société
sur beaucoup de points. Mais pour l'alimentation, qui constitue une
catégorie importante de notre impact individuel, cette réduction par
5 est à la portée de chacun, simplement, dès demain: arrêtez de manger
*régulièrement* du boeuf, du veau ou de l'agneau -- pourquoi pas une
fois de temps en temps, pour les fêtes, chez un ami... mais
exceptionellement. Choisissez plutôt du poulet, du poisson, ou
encore mieux un repas végétarien.

Pour commencer, et pour avoir rapidement un impact collectif au-delà
des démarches strictement individuelles: parlons-en autour de nous, et
demandons-le urgemment à nos restaurations collectives.
