# Structure 

## Question: Vivre ≤ 2T, c'est possible?

## Analyse d'ensemble

- Par secteur
  
- Par type de pollution
  rapport HCC + analyse B
- Locale, extérieure: rapport HCC

## Transports

Plein de données sur les transports en France:
  https://www.statistiques.developpement-durable.gouv.fr/memento-de-statistiques-des-transports-2019

## Alimentation

Plein de chiffres sur la base carbone de l'ADEME. Avant j'avais
cherché des sources différentes pour chaque aliment, là tout est
rassemblé ensemble de façon pertinente pour la France.

Mangue importée par avion: 14.9 kg/kg
Mangue importée par bateau: 0.655 kg/kg

(Il faut retourner chercher les chiffres de transport de biens en
avion, camion et bateau.)

## Alimentation, choix possibles: quels impacts ?

- vegane / vegetarien / viendard
- bio / pas bio
- local / pas local

# Documents

## Analyse des impacts environnementaux de la consommation des ménages et des marges de manoeuvre pour réduire ces impacts.
Bio Intelligence Service, 2011
http://temis.documentation.developpement-durable.gouv.fr/document.html?id=Temis-0076294

Contient une comparaison détaillée des sources de pollution des produits de consommation des ménages.
Attention: la pollution à l'usage n'est pas incluse (pollution des transports, consommation énergétique, etc.)


## Nouveau rapport du HCC sur l’empreinte carbone de la France (2020)
https://www.citepa.org/fr/2020_10_a04/
https://www.hautconseilclimat.fr/wp-content/uploads/2020/10/hcc_rapport_maitriser-lempreinte-carbone-de-la-france-1.pdf

Bonnes données par secteur + découpage local/importé (et provenance de l'importation)
pas de données sur les ménages dans l'infographie


## L'empreinte carbone de la France
https://ree.developpement-durable.gouv.fr/themes/defis-environnementaux/changement-climatique/empreinte-carbone/article/l-empreinte-carbone-de-la-france#label_onglet818

Bonne analyse d'ensemble, avec les produits et aussi les ménages.

Dëfinitions des les catégories comme "cokéfaction et raffinage"
  https://www.insee.fr/fr/metadonnees/nafr2/division/19?champRecherche=false
... mais il reste plein de questions là-dessus.

## Alléger l'empreinte environnemntale de la consommation des français en 2030

Plein de graphiques intéressants.

Multi-pollutions: Figure 14, page 34

